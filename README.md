- [x] Créer la database:
- [x] Créer l'entité Plante.

- [x] Créer un controller pour la page d'accueil
    - [x] Créer une route pour la page d'accueil
    - [x] Créer le template twig pour la page d'accueil
        - [x] intégrer base.html.twig dans le template de l'accueil
        - [x] intégrer une librairie css

- [x] Créer de fausse plante en base de donner a l'aide de fixtures:
    - [x] installer fixtures (composer require --dev orm-fixtures).
    - [x] coder la fixtures pour les plantes.
    
- [x] Créer un controller pour les plante:
    - [x] Faire les CRUD pour les plantes:
        - [x] Ajouté plante,
            -[x] formulaire d'ajout
        - [x] Modifier plante
        - [x] Prendre toute les instences et les retourner dans un tempplate
        - [x] Prendre une instence et la retourner dans un template
        - [x] Delete plante.
        
- [x] Créer l'entité User && InfosUser
    - [x] User
    - [x] InfosUser

- [x] créer le controller de connexion
    - [x] créer formulaire de connexion dans un template twig
- [x] créer le controller d'inscription
    - [x] créer un formulaire d'incription via une class et l'intégré dans un template twig
    
- [x] Ajouté un token de validation par mail pour validé son compte lord de la premiére connexion
- [x] ajouté un token pour changer de mot de passe et créer la route pour l'envoi d'un email contenant ce token pour changer de mot de passe
- [x] ajout d'une route pour changer de mot de passe l'utilisateur y accéde par mail

- [x] Ajouter les tables departement et region
- [x] Ajout d'une relation entre infosUser et regions
- [x] Ajout d'une relation entre infosUer et departement 

- [x] Ajout Page profils 
    - [x] Ajout lien vers page du profil
    
- [X] Ajout formulaire de recherche par departement region plante


- [x] Ajout de vich uploader pour gérer l'upload d'image
