<?php

namespace App\Repository;

use App\Entity\FamilyPlante;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FamilyPlante|null find($id, $lockMode = null, $lockVersion = null)
 * @method FamilyPlante|null findOneBy(array $criteria, array $orderBy = null)
 * @method FamilyPlante[]    findAll()
 * @method FamilyPlante[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FamilyPlanteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FamilyPlante::class);
    }

    // /**
    //  * @return FamilyPlante[] Returns an array of FamilyPlante objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FamilyPlante
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
