<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200511151026 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE infos_user ADD identifiant_id INT NOT NULL');
        $this->addSql('ALTER TABLE infos_user ADD CONSTRAINT FK_AA81A6EA1016936D FOREIGN KEY (identifiant_id) REFERENCES user (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AA81A6EA1016936D ON infos_user (identifiant_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE infos_user DROP FOREIGN KEY FK_AA81A6EA1016936D');
        $this->addSql('DROP INDEX UNIQ_AA81A6EA1016936D ON infos_user');
        $this->addSql('ALTER TABLE infos_user DROP identifiant_id');
    }
}
