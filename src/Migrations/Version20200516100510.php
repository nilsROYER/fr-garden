<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200516100510 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE plante ADD yes_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE plante ADD CONSTRAINT FK_517A69472CB716C7 FOREIGN KEY (yes_id) REFERENCES infos_user (id)');
        $this->addSql('CREATE INDEX IDX_517A69472CB716C7 ON plante (yes_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE plante DROP FOREIGN KEY FK_517A69472CB716C7');
        $this->addSql('DROP INDEX IDX_517A69472CB716C7 ON plante');
        $this->addSql('ALTER TABLE plante DROP yes_id');
    }
}
