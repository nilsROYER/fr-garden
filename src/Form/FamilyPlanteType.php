<?php

namespace App\Form;

use App\Entity\FamilyPlante;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class FamilyPlanteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('familyContent')
            ->add('imageFile', VichImageType::class, [
                'attr'         => ['placeholder' => 'Choisir une photos'],
                'delete_label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FamilyPlante::class,
        ]);
    }
}
