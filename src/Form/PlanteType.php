<?php

namespace App\Form;

use App\Entity\FamilyPlante;
use App\Entity\Plante;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PlanteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('description')
            ->add('family', EntityType::class, [
                'class'        => FamilyPlante::class,
                'choice_label' => 'name',
                'label' => 'famille'
            ])
            ->add('imageFile', VichImageType::class, [
                'label'        => 'Image',
                'attr'         => ['placeholder' => 'Choisir une photos'],
                'delete_label' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Plante::class,
        ]);
    }
}
