<?php

namespace App\Form;

use App\Entity\Departement;
use App\Entity\InfosUser;
use App\Entity\Regions;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InfosUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('pseudo')
            ->add('region', EntityType::class, [
                'class'        => Regions::class,
                'choice_label' => 'name',
                'required'     => false,
                'placeholder'  => 'Votre région ...',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => InfosUser::class,
        ]);
    }
}
