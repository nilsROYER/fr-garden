<?php

namespace App\DataFixtures;

use App\Entity\Plante;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PlanteFixtures extends Fixture
{
    public function load( ObjectManager $manager )
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 20; $i++) {
            $plante = new Plante();

            $plante->setNom($faker->name())
                   ->setDescription($faker->paragraph())
                   ->setImage($faker->imageUrl(150, 150, 'nature'))
                   ->setCreateAt($faker->dateTime('-3 months'));

            $manager->persist($plante);
        }

        $manager->flush();
    }
}
