<?php


namespace App\Controller;


use App\Entity\FamilyPlante;
use App\Entity\InfosUser;
use App\Form\FamilyPlanteType;
use App\Form\InfosUserType;
use App\Repository\InfosUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @param InfosUserRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="page_admin")
     */
    public function admin(InfosUserRepository $repository)
    {
        // Si l'utilisateur n'a pas le rôle admin
        if (!$this->isGranted('ROLE_ADMIN')) {
            // On le redirige sur la page d'acceil
            return $this->redirectToRoute('accueil');
        }
        // On renvoie un templates est un tableau d'objet contenant tous nos User
        return $this->render('admin/page-admin.html.twig', [
            'profils' => $repository->findAll()
        ]);
    }

    /**
     * @Route("/ban/{id}", name="bannir_profile", requirements={"id":"\d+"})
     */
    public function bannir(InfosUser $userInfos)
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('accueil');
        }
        $user    = $userInfos->getIdentifiant()->setRoles(['ROLE_BANNI']);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();

        return $this->redirectToRoute('page_admin');
    }

    /**
     * @Route("/debannir/{id}", name="debannir_profile")
     */
    public function debannir(InfosUser $userInfos)
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('accueil');
        }
        $user    = $userInfos->getIdentifiant()->setRoles(['ROLE_VALID']);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();

        return $this->redirectToRoute('page_admin');
    }

    /**
     * @Route("admin/profile-modif/{id}", name="modif_profile", requirements={"id":"\d+"})
     */
    public function modifProfile(InfosUser $info, Request $request)
    {

        if (!$this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('accueil');
        }
        $modifForm = $this->createForm(InfosUserType::class, $info)->handleRequest($request);

        if ($modifForm->isSubmitted() && $modifForm->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($info);
            $manager->flush();
            return $this->redirectToRoute('profil_garden', ['id' => $info->getId()]);
        }
        return $this->render('admin/modif.html.twig', [
            'modifForm' => $modifForm->createView(),
        ]);
    }

    /**
     * @Route("/categories/new", name="ajout_categorie")
     */
    public function addCategorie(Request $request, EntityManagerInterface $manager)
    {

        if ( ! $this->isGranted('ROLE_ADMIN')){
            return $this->redirectToRoute('accueil');
        }

        $categorie = new FamilyPlante();
        $form      = $this->createForm(FamilyPlanteType::class, $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($categorie);
            $manager->flush();

            return $this->redirectToRoute('accueil');
        }

        return $this->render('admin/categories/new.html.twig', [
            'categorieForm' => $form->createView(),
        ]);


    }
}
