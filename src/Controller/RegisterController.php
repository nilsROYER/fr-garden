<?php


namespace App\Controller;

use App\Entity\InfosUser;
use App\Entity\User;
use App\Form\RegisterType;
use App\Repository\UserRepository;
use App\Security\Authenticator;
use App\Service\RegisterService;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;


class RegisterController extends AbstractController
{

    /**
     * @var FlashyNotifier
     */
    private $flashy;

    /**
     * @var RegisterService
     */
    private $registerService;


    /**
     * RegisterController constructor.
     * @param FlashyNotifier $flashy
     * @param RegisterService $registerService
     */
    public function __construct(FlashyNotifier $flashy, RegisterService $registerService)
    {
        $this->flashy          = $flashy;
        $this->registerService = $registerService;
    }

    /**
     * @Route("/register", name="app_register")
     *
     * @param Request $request
     * @param RegisterService $registerService
     * @return Response
     */
    public function registration(Request $request): Response
    {
        // On verifie que l'utilisateur ne sois pas connecter
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $this->flashy->primaryDark('Vous étes déjà connecter donc vous avez déjà un compte');
            return $this->redirectToRoute('accueil');
        }

        // On creer un utilisateur
        $user = new User();
        // On creer infosUser qui est un objet qui a une relation avec user
        // et qui va contenir des donnée personel information et ses plante
        $infosUser = new InfosUser();

        // On creer le formulaire d'inscription et on le lis a notre user vide
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        // Si le formulaire est soumis et que c'est données sont valide
        if ($form->isSubmitted() && $form->isValid()) {

            // On appel le service d'inscription (RegisterService) pour enregistré le nouvelle utilisateur
            $this->registerService->newUser($user, $infosUser);

            // Pour le moment on redirige l'utilisateur directement sur la route qui valide le compte
            // mais ce systéme a pour but une validation par mail
            return $this->redirectToRoute('activation', ['token' => $user->getActivationToken()]);

        }


        return $this->render("register/register.html.twig", [
            'registerForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/activation/{token}", name="activation")
     *
     * @param $token
     * @param Request $request
     * @param Authenticator $authenticator
     * @param GuardAuthenticatorHandler $guardHandler
     * @param UserRepository $repos
     * @return Response|null
     */
    public function activation($token, Request $request, Authenticator $authenticator, GuardAuthenticatorHandler $guardHandler, UserRepository $repos)
    {

        // On va chercher le compte grace au token
        $user = $repos->findOneBy(['activation_token' => $token]);

        // Si aucun utilisateur existe avec ce token
        if (!$user) {
            // error 404
            throw $this->createNotFoundException("Cet utilisateur n'existe pas");
        }
        // On Appel le service d'inscription {RegisterService} pour validé le comppte
        $this->registerService->validationUser($token, $user);
        // On redirige vers l'accueil
        return $guardHandler->authenticateUserAndHandleSuccess( // Apprés la validation cette methode permet de connecter directement l'utilisateur a son compte
            $user,
            $request,
            $authenticator,
            'main'
        );

    }
}
