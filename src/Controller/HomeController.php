<?php

namespace App\Controller;

use App\Entity\InfosUser;
use App\Entity\Plante;
use App\Entity\Regions;
use App\Entity\User;
use App\Form\InfosUserType;
use App\Service\UserService;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @Route("/", name="accueil")
     */
    public function profils(Request $request)
    {
        // On crée une variable form qui va contenir notre formulaire de trie par regions
        $form = $this->createFormBuilder()
            ->add('region', EntityType::class, [
                'class'       => Regions::class,
                'required'    => false,
                'placeholder' => 'Trier par régions'
            ])
            ->getForm();
        // Ont permet a notre formulaire de traiter des requetes
        $form->handleRequest($request);

        // Si le formulaire est soumis et que les données saisies sont valide
        if ($form->isSubmitted() && $form->isValid()) {
            // On stock la requete dans un tableau [$data]
            $data = $form->getData();

            // On renvoie un template twig avec les profil trier par regions et un formulaire
            return $this->render('accueil/home.html.twig', [
                'profils' => $this->userService->selectBy('region', $data['region']),
                'form'    => $form->createView(),
            ]);
        }

        return $this->render('accueil/home.html.twig', [
            'profils' => $this->userService->allUser(),
            'form'    => $form->createView(),
        ]);
    }


    /**
     * @Route("/moncompte", name="page_infosCompte")
     * @Route("/moncompte/{id}", name="page_infosCompte_plante")
     */
    public function monCompte(Request $request, FlashyNotifier $flashy, Plante $selectPlante = null)
    {
        // Si l'utilisateur n'a pas le role VALID
        if (!$this->isGranted('ROLE_VALID')) {
            // On envoie un message à l'utilisateur
            $flashy->primaryDark('Validé votre compte pour accéder à cette page');
            // On redirige l'utilisateur sur l'accueil
            return $this->redirectToRoute('accueil');
        }

        // Si l'utilisateur est un admin
        if ($this->isGranted('ROLE_ADMIN')) {
            // On le dirige sur sa page d'administration
            return $this->redirectToRoute('page_admin');
        }

        // On creer une variable form qui contient un formulaire basé sur les InfosUser et on lui demande d'écouter les requete http
        $form = $this->createForm(InfosUserType::class, $this->getUser()->getInfosUser())->handleRequest($request);

        // Si le formulaire et soumis et que les données saisie sont valide
        if ($form->isSubmitted() && $form->isValid()) {

            // On appel le service pour les user et on utilise la methode pour modifier les information
            $this->userService->modifInfos();

        }

        return $this->render('security/moncompte.html.twig', [
            'infosUserForm' => $form->createView(),
            'user'          => $this->getUser(),
            'plante'        => $selectPlante,
        ]);

    }


    /**
     * @Route("/profil/{id}", name="profile_jardin", requirements={"id":"\d+"})
     */
    public function profilGarden(InfosUser $infosUser)
    {
        return $this->render('profils/profil.html.twig', [
            'profil' => $infosUser,
        ]);
    }

}
