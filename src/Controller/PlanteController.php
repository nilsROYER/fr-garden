<?php

namespace App\Controller;

use App\Entity\Plante;
use App\Form\PlanteType;
use App\Service\PlanteService;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PlanteController extends AbstractController
{
    /**
     * @var FlashyNotifier
     */
    private $flashy;
    /**
     * @var PlanteService
     */
    private $planteService;


    public function __construct(FlashyNotifier $flashy, PlanteService $planteService)
    {
        $this->flashy = $flashy;
        $this->planteService = $planteService;
    }

    /**
     * @Route("/plante/add", name="ajouter_plante")
     */
    public function add(Request $request)
    {

        if (!$this->isGranted('ROLE_VALID')) {
            $this->flashy->primaryDark('Validé votre compte pour accéder à cette page');
            return $this->redirectToRoute('accueil');
        }

        $plante = new Plante();
        $form   = $this->createForm(PlanteType::class, $plante)->handleRequest($request);

        // Si le formulaire est soumis et que les données sont valide
        if ($form->isSubmitted() && $form->isValid()) {

            // On appelle le service pour les plantes et on utilise la méthode add pour ajouter la plante
           $this->planteService->add($plante);

            // On redirige sur la route showSingle avec l'id de notre nouvelle plante
            return $this->redirectToRoute('page_infosCompte', ['id' => $plante->getId()]);

        }
        return $this->render('plante/add.html.twig', [
            'planteForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/plante/{id}/update", name="updatePlante", requirements={"id":"\d+"})
     */
    public function update(Plante $plante, Request $request)
    {
        // On verifie que le l'utilisateur ait bien les droit pour emprunter cette route
        if (!$this->isGranted('ROLE_VALID')) {
            $this->flashy->primaryDark('Validé votre compte pour accéder à cette page');
            return $this->redirectToRoute('home');
        }
        // On créer une variable $form qui contient un formulaire pour les plante est lui passe la plante selectionné en argument
        $form = $this->createForm(PlanteType::class, $plante)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // On appel le service pour les plante et on utilise la method update pour modifier la plante
            $this->planteService->update($plante);

            // on redirige sur la route showSingle avec l'id de notre nouvelle plante
            return $this->redirectToRoute('page_infosCompte_plante', ['id' => $plante->getId()]);
        }

        return $this->render('plante/update.html.twig', [
            'planteForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/plante/{id}/delete", name="deletePlante", requirements={"id":"\d+"})
     */
    public function delete(Plante $plante)
    {
        if (!$this->isGranted('ROLE_VALID')) {
            $this->flashy->primaryDark('Validé votre compte pour accéder à cette page');
            return $this->redirectToRoute('home');
        }

        // On appel le service pour les plantes et on lui passe la methode delete pour supprimer la plante
        $this->planteService->delete($plante);

        return $this->redirectToRoute('page_infosCompte');
    }
}
