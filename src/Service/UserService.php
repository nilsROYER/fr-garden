<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\InfosUserRepository;
use Doctrine\ORM\EntityManagerInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\Security\Core\Security;


class UserService
{
    /**
     * @var InfosUserRepository
     */
    private $repos;

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var FlashyNotifier
     */
    private $flashy;
    /**
     * @var Security
     */
    private $security;

    public function __construct(infosUserRepository $repos, EntityManagerInterface $manager, FlashyNotifier $flashy, Security $security)
    {
        $this->repos    = $repos;
        $this->manager  = $manager;
        $this->flashy   = $flashy;
        $this->security = $security;
    }

    /**
     * Renvoie tout les compte
     */
    public function allUser()
    {
        return $this->repos->findAll();
    }

    /**
     * Renvoie tout les comptes en fonction de la région
     * @param $key
     * @param $value
     * @return \App\Entity\InfosUser[]
     */
    public function selectBy($key, $value)
    {
        return $this->repos->findBy([$key => $value]);
    }

    /**
     * Modif infos user
     */
    public function modifInfos()
    {
        // On enregistre les modification
        $this->manager->persist($this->security->getUser()->getInfosUser());
        $this->manager->flush();
        $this->flashy->success('vous avez bien enregistré vos information');
    }

    /**
     * Supprimer un compte
     * @param User $user
     */
    public function deleteUser(User $user)
    {
        // On va chercher toute les plante de l'utilisateur [tableau d'objets]
        $garden = $user->getInfosUser()->getGarden();
        // On boucle sur le tableau d'objets (il représente le jardin de l'utilisateur)
        foreach ($garden as $plante) {
            // on supprime la plante
            $this->manager->remove($plante);
        }

        // On supprime l'utilisateur
        $this->manager->remove($user);
        // On valide la supprécions du compte
        $this->manager->flush();
    }
}
