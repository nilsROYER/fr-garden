<?php


namespace App\Service;


use App\Entity\InfosUser;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @method createNotFoundException(string $string)
 */
class RegisterService
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var FlashyNotifier
     */
    private $flashy;

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var UserRepository
     */
    private $repos;



    public function __construct(UserRepository $userRepo, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder,
                                FlashyNotifier $flashy, UserRepository $repos)
    {
        $this->manager       = $manager;
        $this->encoder       = $encoder;
        $this->flashy        = $flashy;
        $this->userRepo      = $userRepo;
        $this->repos         = $repos;
    }

    /**
     * fonction pour creer un nouvelle utilisateur
     * @param User $user
     * @param InfosUser $infosUser
     * @throws \Exception
     */
    public function newUser(User $user, InfosUser $infosUser)
    {
        // On chiffre le mot de passe
        $infosUser->setIdentifiant($user)
            ->setCreatedAt(new \DateTime());
        $hash = $this->encoder->encodePassword($user, $user->getPassword());
        // On remplace le mot de passe par sa version chiffré
        $user->setPassword($hash)
            ->setRoles(['ROLE_USER'])
            ->setInfosUser($infosUser);

        // On génère le token d'activation
        $user->setActivationToken(md5(uniqid())); // md5 : algorithme de chiffrement, uniqid : algorithm pour avoir des id unique


        $this->manager->persist($user);
        $this->manager->flush();

        // On envoie un mail pour vérifier le compte le mail contien un lien d'activation
        // On créer le message
//            $message = (new \Swift_Message('Activation de votre compte'))
//                // On attribue l'ecpéditeur le site
//                ->setFrom('site@site.fr')
//                // On attribue le destinataire
//                ->setTo($user->getEmail())
//                // On créer le contenue
//                ->setBody(
//                    $this->renderView('email/activation.html.twig', [
//                        'token' => $user->getActivationToken(),
//                    ]),
//                    'text/html'
//                );
//            // On envoie le message dans un mail
//            $mailer->send($message);
        // On prévient l'utilisateur qu'il a reçus un email de validation
//            $this->flashy->message('un email de verification vous a été envoyer');

    }

    /**
     * Fonction de validation
     * @param $token
     * @param User $user
     */
    public function validationUser($token, User $user)
    {
        // On efface son token d'activation
        $user->setActivationToken(null);
        // On change son role
        $user->setRoles(['ROLE_VALID']);
        // On demande aux manager d'enregistré la nouvelle plante
        $this->manager->persist($user);
        // On demande au manager d'enregistré la nouvelle plante en base de données
        $this->manager->flush();
        // On envoie un message Flash
        $this->flashy->success('vous avez bien activé votre compte');

    }
}
