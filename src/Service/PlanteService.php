<?php


namespace App\Service;

use App\Entity\Plante;
use App\Repository\PlanteRepository;
use Doctrine\ORM\EntityManagerInterface;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Component\Security\Core\Security;

class PlanteService
{
    /**
     * @var PlanteRepository
     */
    private $repos;
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var FlashyNotifier
     */
    private $flashy;
    /**
     * @var Security
     */
    private $security;

    public function __construct(PlanteRepository $repos, EntityManagerInterface $manager, FlashyNotifier $flashy, Security $security)
    {
        $this->repos    = $repos;
        $this->manager  = $manager;
        $this->flashy   = $flashy;
        $this->security = $security;
    }

    /**
     * Fonction pour modifier des données en base de donnée je l'utilise pour refactoriser mon code
     * @param $object
     */
    public function persistObject($object)
    {
        // On demande aux manager d'enregistré la nouvelle plante
        $this->manager->persist($object);
        // On demande au manager d'enregistré la nouvelle plante en base de données
        $this->manager->flush();
    }

    /**
     * affichez toutes les plantes
     * @return Plante[]
     */
    public function showAll()
    {
        return $this->repos->findAll();
    }

    /**
     * Ajouter une plante
     * @param Plante $plante
     * @throws \Exception
     */
    public function add(Plante $plante)
    {
        $user = $this->security->getUser()->getInfosUser();
        // On ajoute la date de création de la plante
        $plante->setCreateAt(new \DateTime());
        // On lie l'utilisateur connecter à la plante qu'il créer
        $plante->setYes($user);

        // On appel notre function pour enregistrer en base de donnée la plante
        $this->persistObject($plante);

        // on envoie un message à l'utilisateur
        $this->flashy->primaryDark('Vous venez d\'ajouté un plante a votre jardin');

    }

    /**
     * Modifier une plante
     * @param Plante $plante
     */
    public function update(Plante $plante)
    {
        $this->persistObject($plante);
    }


    /**
     * Supprimer une plante
     * @param Plante $plante
     */
    public function delete(Plante $plante)
    {
        // On demande au manager de supprimer la plante
        $this->manager->remove($plante);
        // On valid la supprécions
        $this->manager->flush();

        // On envoie un message à l'utilisateur
        $this->flashy->info('Vous avez bien suprimer une plante');
    }
}

