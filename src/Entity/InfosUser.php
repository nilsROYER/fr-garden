<?php

namespace App\Entity;

use App\Repository\InfosUserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass=InfosUserRepository::class)
 *
 */
class InfosUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="infosUser", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(nullable=false)
     */
    private $identifiant;

    /**
     * @ORM\OneToMany(targetEntity=Plante::class, mappedBy="yes")
     */
    private $garden;

    /**
     * @ORM\ManyToOne(targetEntity=Regions::class, inversedBy="yes")
     */
    private $region;



    public function __construct()
    {
        $this->garden = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
            return htmlspecialchars($this->nom);

    }

    public function setNom(?string $nom): self
    {
            $this->nom = htmlspecialchars($nom);
            return $this;
    }

    public function getPrenom(): ?string
    {
        return htmlspecialchars($this->prenom);
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = htmlspecialchars($prenom);

        return $this;
    }

    public function getPseudo(): ?string
    {
        return htmlspecialchars($this->pseudo);
    }

    public function setPseudo(?string $pseudo): self
    {
        $this->pseudo = htmlspecialchars($pseudo);

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIdentifiant(): ?User
    {
        return $this->identifiant;
    }

    public function setIdentifiant(User $identifiant): self
    {
        $this->identifiant = $identifiant;

        return $this;
    }

    /**
     * @return Collection|Plante[]
     */
    public function getGarden(): Collection
    {
        return $this->garden;
    }

    public function addGarden(Plante $garden): self
    {
        if (!$this->garden->contains($garden)) {
            $this->garden[] = $garden;
            $garden->setYes($this);
        }

        return $this;
    }

    public function removeGarden(Plante $garden): self
    {
        if ($this->garden->contains($garden)) {
            $this->garden->removeElement($garden);
            // set the owning side to null (unless already changed)
            if ($garden->getYes() === $this) {
                $garden->setYes(null);
            }
        }

        return $this;
    }

    public function getRegion(): ?Regions
    {
        return $this->region;
    }

    public function setRegion(?Regions $region): self
    {
        $this->region = $region;

        return $this;
    }




}
