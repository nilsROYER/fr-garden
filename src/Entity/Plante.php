<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanteRepository")
 * @Vich\Uploadable()
 */
class Plante
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\Length(min="3", max="30", minMessage="Le nom doit faire entre 3 et 30 caractéres")
     */
    private $nom;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @Assert\Length(min="15", minMessage="Votre description doit faire aux moins 15 caractéres")
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="plante", fileNameProperty="image")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTimeInterface|null
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createAt;

    /**
     * @ORM\ManyToOne(targetEntity=InfosUser::class, inversedBy="garden")
     */
    private $yes;

    /**
     * @ORM\ManyToOne(targetEntity=FamilyPlante::class, inversedBy="yes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $family;


    public function __construct()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return htmlspecialchars($this->nom);
    }

    public function setNom(string $nom): self
    {
        $this->nom = htmlspecialchars($nom);

        return $this;
    }

    public function getDescription(): ?string
    {
        return htmlspecialchars($this->description);
    }

    public function setDescription(string $description): self
    {
        $this->description = htmlspecialchars($description);

        return $this;
    }


    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeInterface $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getYes(): ?InfosUser
    {
        return $this->yes;
    }

    public function setYes(?InfosUser $yes): self
    {
        $this->yes = $yes;

        return $this;
    }


    /**
     * for vich uploader method
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     * @throws \Exception
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return htmlspecialchars($this->image);
    }

    /**
     * @param string|null $image
     */
    public function setImage(?string $image): self
    {
        $this->image = htmlspecialchars($image);

        return $this;
    }

    public function getFamily(): ?FamilyPlante
    {
        return $this->family;
    }

    public function setFamily(?FamilyPlante $family): self
    {
        $this->family = $family;

        return $this;
    }


}
