<?php

namespace App\Entity;

use App\Repository\FamilyPlanteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass=FamilyPlanteRepository::class)
 * @Vich\Uploadable()
 */
class FamilyPlante
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $familyContent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $familyLogo;

    /**
     * @Vich\UploadableField(mapping="planteLogo", fileNameProperty="familyLogo")
     * @var File
     */
    private $imageFile;

    /**
     * @var \DateTime $ updatedAt
     * @ORM\Column(type="datetime", nullable=true)
     *
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity=Plante::class, mappedBy="family", orphanRemoval=true)
     */
    private $yes;


    public function __construct()
    {
        $this->yes = new ArrayCollection();
        $this->updatedAt = new \DateTime();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFamilyContent(): ?string
    {
        return $this->familyContent;
    }

    public function setFamilyContent(string $familyContent): self
    {
        $this->familyContent = $familyContent;

        return $this;
    }



    /**
     * @return Collection|Plante[]
     */
    public function getYes(): Collection
    {
        return $this->yes;
    }

    public function addYe(Plante $ye): self
    {
        if (!$this->yes->contains($ye)) {
            $this->yes[] = $ye;
            $ye->setFamily($this);
        }

        return $this;
    }

    public function removeYe(Plante $ye): self
    {
        if ($this->yes->contains($ye)) {
            $this->yes->removeElement($ye);
            // set the owning side to null (unless already changed)
            if ($ye->getFamily() === $this) {
                $ye->setFamily(null);
            }
        }

        return $this;
    }


    /**
     * for vich uploader method
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     * @throws \Exception
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getFamilyLogo(): ?string
    {
        return $this->familyLogo;
    }

    public function setFamilyLogo(string $familyLogo): self
    {
        $this->familyLogo = $familyLogo;

        return $this;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
